###############################################################################
# SRE
#
# Crackme files for learning gdb

C      = gcc

TARGETS = random_01_dbg random_03_dbg segfault_01_dbg_O random_02_dbg segfault_01_dbg segfault_02_dbg

all: $(TARGETS)

random_01_dbg: random_01.c
	$(C) -g     -o $@   $<

random_03_dbg: random_03.c
	$(C) -g     -o $@   $<

random_02_dbg: random_02.c
	$(C) -g     -o $@   $<

segfault_01_dbg_O: segfault_01.c
	$(C) -g -O3 -o $@ $<

segfault_01_dbg: segfault_01.c
	$(C) -g     -o $@   $<

segfault_02_dbg: segfault_02.c
	$(C) -g     -o $@   $<

clean:
	rm -f $(TARGETS)
