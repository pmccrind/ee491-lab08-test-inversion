///////////////////////////////////////////////////////////////////////////////
/// SRE
///
/// A harder random number crackme

#include <stdio.h>
#include <stdlib.h>

int getRandom() {
   int i = random();
   return i;
}

int unlock() {
	// 1. Get the user's password
   int entered_pw;
   printf( "Enter the password: " );
   fflush(stdout);
   scanf("%d", &entered_pw );

	// 2. Get the target password
   int actual_pw;
   actual_pw = getRandom();

	// 3. Do the test
   if( entered_pw == actual_pw ) {
      printf( "Access granted: code (%d)\n", getRandom() );
      return 0;
   } else if (entered_pw != actual_pw ) {
      printf( "Access denied\n" );
      return 1;
   }
}

int main() {
   return unlock();
}
