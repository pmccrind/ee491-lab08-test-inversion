///////////////////////////////////////////////////////////////////////////////
/// SRE
///
/// Generate a segfault -- so we can look at stack frames in gdb

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int getRandom() {
   int i = random() % 100;
   return i;
}

void segfaultSometimes() {
   int i;
   i = getRandom();

   if( i == 50 ) {
      char* badPtr = (char*) 0x100;
      *badPtr = '!';
   }

   segfaultSometimes();
}

int main() {
	srand(42);
   segfaultSometimes();

   return -1;
}

